package src;

public class JavaDataTypes2 {
    public static void main(String[] args) {
        byte myNumByte = 100;
        short myNumShort = 5000;
        int myNumInt = 100000;
        long myNumLong = 15000000000L;
        float myNumFloat = 5.75f;
        double myNumDouble = 19.99d;

        System.out.println(myNumByte);
        System.out.println(myNumShort);
        System.out.println(myNumInt);
        System.out.println(myNumLong);
        System.out.println(myNumFloat);
        System.out.println(myNumDouble);
    }
}
