package src;

public class JavaDataTypes3 {
    public static void main(String[] args) {
        float f1 = 35e3f;
        double d1 = 12E4d;
        boolean isJavaFun = true;
        boolean isFishTasty = false;
        char myGrade = 'B';
        char myVar1 = 65, myVar2 = 66, myVar3 = 67;
        String greeting = "Hello World";

        
        System.out.println(f1);
        System.out.println(d1);
        System.out.println(isJavaFun); // Outputs true
        System.out.println(isFishTasty); // Outputs false
        System.out.println(myGrade);
        System.out.println(myVar1);
        System.out.println(myVar2);
        System.out.println(myVar3);
        System.out.println(greeting);
    }
}
