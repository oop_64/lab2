package src;

public class JavaStrings8 {
    public static void main(String[] args) {
        String x = "10";
        String y = "20";
        String z = x + y; // z will be 1020 (a String)
        System.out.println(z);
    }

}
