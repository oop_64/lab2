package src;

public class JavaComments2 {
    public static void main(String[] args) {
        /* The code below will print the words Hello World
        to the screen, and it is amazing */
        System.out.println("Hello World");
    }
    
}
