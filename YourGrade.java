import java.util.Scanner;
public class YourGrade {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("please enter a number = ");
        int point = sc.nextInt();


        if(point <= 100 && point >= 80){
            System.out.println("A");
        }
        else if (point < 80 && point >= 75 ){
            System.out.println("B+");
        }
        else if (point < 75 && point >= 70 ){
            System.out.println("B");
        }
        else if (point < 70 && point >= 65 ){
            System.out.println("C+");
        }
        else if (point < 65 && point >= 60 ){
            System.out.println("C");
        }
        else if (point < 60 && point >= 55 ){
            System.out.println("D+");
        }
        else if (point < 55 && point >= 50 ){
            System.out.println("D");
        } 
        else if (point < 50) {
            System.out.println("F");
        }
    }
}
